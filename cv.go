package main

import (
	"io/ioutil"
	"log"
	"os"
	"strings"
	"text/template"

	yaml "gopkg.in/yaml.v2"
)

const (
	DataFile     = "cv.yaml"
	TemplateFile = "template/latex.gotmpl"
)

type CV struct {
	Meta            *Meta             `yaml:"meta"`
	Info            *Info             `yaml:"info"`
	Summary         string            `yaml:"summary"`
	Accomplishments []*Accomplishment `yaml:"accomplishments"`
	Experience      []*Experience     `yaml:"experience"`
	Skills          *Skills           `yaml:"skills"`
	Education       []*Education      `yaml:"education"`
	Communication   *Communication    `yaml:"communication"`
}

type Communication struct {
	Render  bool
	Bullets []string
}

type Meta struct {
	Color           string `yaml:"color"`
	Picture         bool   `yaml:"picture"`
	Coverletter     string `yaml:"coverletter"`
	CoverletterFile string `yaml:"coverletterFile"`
}

type Info struct {
	FirstName    string `yaml:"firstName"`
	LastName     string `yaml:"lastName"`
	Title        string `yaml:"title"`
	Phone        string `yaml:"phone"`
	AddressLine1 string `yaml:"addressLine1"`
	AddressLine2 string `yaml:"addressLine2"`
	Email        string `yaml:"email"`
	Website      string `yaml:"website"`
}

type Accomplishment struct {
	Headline string   `yaml:"headline"`
	Summary  string   `yaml:"summary"`
	Bullets  []string `yaml:"bullets"`
}

type Experience struct {
	Time        string   `yaml:"time"`
	Position    string   `yaml:"position"`
	Company     string   `yaml:"company"`
	Description string   `yaml:"description"`
	Bullets     []string `yaml:"bullets"`
}

type Skills struct {
	Languages    []string `yaml:"languages" json:"languages"`
	Databases    []string `yaml:"databases" json:"databases"`
	Platforms    []string `yaml:"platforms" json:"platforms"`
	Frameworks   []string `yaml:"frameworks" json:"frameworks"`
	Editors      []string `yaml:"editors" json:"editors"`
	Tools        []string `yaml:"tools" json:"tools"`
	Environments []string `yaml:"environments" json:"environments"`
	Hypervisors  []string `yaml:"hypervisors" json:"hypervisors"`
}

type Education struct {
	School string `yaml:"school" json:"school"`
	City   string `yaml:"city" json:"city"`
	Degree string `yaml:"degree" json:"degree"`
	Date   string `yaml:"date" json:"date"`
}

func main() {
	// read in CV information
	data, err := ioutil.ReadFile(DataFile)
	if err != nil {
		log.Fatal(err)
	}
	var c CV
	if err := yaml.Unmarshal(data, &c); err != nil {
		log.Fatal(err)
	}

	// if a cover letter is provided render bring it in
	if c.Meta.CoverletterFile != "" {
		letter, err := ioutil.ReadFile(c.Meta.CoverletterFile)
		if err != nil {
			log.Fatal("could not read coverletter:", err)
		}
		c.Meta.Coverletter = string(letter)
	}

	// read in and build template
	b, err := ioutil.ReadFile(TemplateFile)
	if err != nil {
		log.Fatal(err)
	}
	t := template.Must(template.New("latex").
		Delims("((", "))"). // avoid {{ }} because tex uses them a lot
		Funcs(template.FuncMap{"list": list}).
		Parse(string(b)))

	// render template
	t.Execute(os.Stdout, c)
}

func list(lst []string) string { return strings.Join(lst, ", ") }

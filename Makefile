PDFLATEX ?= /Library/TeX/texbin/pdflatex

latex: build
	@mkdir -p output
	./cv |  pdflatex --output-directory=output
	@cp output/texput.pdf cv.pdf

build:
	go build

clean:
	@rm -f cv
	@rm -rf output
